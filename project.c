//ajanvaraus

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include "project.h"
// A kuvaus kuukausi päivä tunti esim A hiustenleikkuu 3 26 14
//jos on jo aiemmin varattu, error
//käytetään scanf, if, lisää ajanvarausjärjestelmään
//kysyy uudelleen tapahtuman ja ajan, kunnes käyttäjä antaa hyväksyttävän ajan

struct what_to_do *funktio_A(struct what_to_do *y, char *tiedostosta)
{
    int i = 0;
    while (y[i].a[0] != 0)
    {
        i++;
    }
    int seuraavan_indksi = i;
    int new_i = i;
    //+2 koska halutaan alustaa seuraava järkevä kohta sekä alustaa sitä seuraava kohta nollaksi merkitsemään taulukon loppua
    y = realloc(y, ((seuraavan_indksi + 2) * sizeof(struct what_to_do)));

    if (tiedostosta != NULL)
    {
        sscanf(tiedostosta, "%s %d %d %d\n", (y[new_i].a), &(y[new_i].appointment.month), &(y[new_i].appointment.day), &(y[new_i].appointment.time));
        y[new_i + 1].a[0] = 0;
    }
    else
    {
        scanf("%s %d %d %d", (y[i].a), &(y[i].appointment.month), &(y[i].appointment.day), &(y[i].appointment.time));
        int was_it_double = 0;
        int j;

        for (j = 0; j < i; ++j)
        {
            if(y[j].appointment.month == y[i].appointment.month && y[j].appointment.day == y[i].appointment.day && y[j].appointment.time == y[i].appointment.time){
                        was_it_double = -1;
                    }
            else 
                was_it_double = was_it_double;
        }

        if ((y[i].appointment.month < 1) || (y[i].appointment.month > 12))
        {
            printf("Please put a valid month.\n");
            y[i] = y[i +1];
            y[i].a[0] = 0;
            printf("Please choose different event and date or delete the event before:\n");
        }
        else if ((y[i].appointment.day < 1) || (y[i].appointment.day > 31))
        {
            printf("Please put a valid date.\n");
            y[i] = y[i +1];
            y[i].a[0] = 0;
            printf("Please choose different event and date or delete the event before:\n");  
        }
        else if ((y[i].appointment.time < 1) || (y[i].appointment.time > 24))
        {
            printf("The time you're trying to choose is not valid.\n");
            y[i] = y[i +1];
            y[i].a[0] = 0;
            printf("Please choose different event and date or delete the event before:\n");
        }
        else if(was_it_double == -1){
            printf("This timeslot is already booked.\n");
            y[i] = y[i +1];
            y[i].a[0] = 0;
            printf("Please choose different event and date or delete the event before:\n");
            was_it_double = 0;
        }
        else 
            y[i +1].a[0] = 0;
    }

    /*for (int j = 0; y[j].a[0] != 0; j++)
    {
        printf("%s %d %d %d\n", y[j].a, y[j].appointment.month, y[j].appointment.day, y[j].appointment.time);
        y[i + 1].a[0] = 0;
    }*/


    return y;
}

// D kuukausi päivä tunti, poistaa tän aikaisen tapahtuman
//jos ei merkintää, virheilmoitus (ei kysy uudelleen vaan palaa mainiin)

struct what_to_do *funktio_D(struct what_to_do *x)
{
    int m, d, t;
    scanf("%d %d %d", &(m), &(d), &(t));
    int k = 0;
    int i;
    int j = 0;
    //int run = 1;

    while(x[k].a[0] != '\0'){
        k++;}

    for ( i = 0; x[i].a[0] != '\0'; i++)
        {

        if (m == x[i].appointment.month && d == x[i].appointment.day && t == x[i].appointment.time)
        {
            for (int m = i; m < k; m++){
                x[m] = x[m+1];
            }
            break;
           
        }
        else 
            j++;
            

        }
    if (j == k) {
        printf("Error. There is no time booked.\n");
        }


    /*for (int j = 0; x[j].a[0] != '\0'; j++)
    {
        printf("%s %d %d %d\n", x[j].a, x[j].appointment.month, x[j].appointment.day, x[j].appointment.time);
    }*/
    return x;
}

//L, tulostaa kalenterin, tulostaa varaukset merkintäajan mukaisessa järjestyksessä, varhaisin ekana myöhäisin vikana
//kuvaus XX, YY, klo ZZ, XX = päivä, YY = kuukausi
//eli lähetään liikkeelle ensin kuukaudesta, sitten päivästä ja lopuksi verrataan aikoja
int funktio_L(const void *z, const void *y)
{
    const struct what_to_do *first_date = z;
    const struct what_to_do *second_date = y;
    
    if ((first_date->appointment.month < second_date->appointment.month) || (first_date->appointment.month > second_date->appointment.month))
    {
        return (first_date->appointment.month - second_date->appointment.month);
    }
    else
    {
        if ((first_date->appointment.day < second_date->appointment.day) || (first_date->appointment.day > second_date->appointment.day))
        {
            return (first_date->appointment.day - second_date->appointment.day);
        }
        else 
            if ((first_date->appointment.time < second_date->appointment.time) || (first_date->appointment.time > second_date->appointment.time))
            {
                return first_date->appointment.time - second_date->appointment.time;
            }
            else 
                return 0;
    }
        
}
        /*if (first_date->appointment.day == second_date->appointment.day)
        {
            if (first_date->appointment.time < second_date->appointment.time)
                return -1;
        }*/



//W tiedostonimi, tallentaa kalenterin, kaikki ajanvaraukset tiedostoon
//write tiedostoon, avata, sulkea tiedosto
//fprintf
int funktio_W(char *filename, struct what_to_do *buffer)
{
    FILE *fp = fopen(filename, "w");
    if (!fp)
    {
        printf("error");
        return -1;
    }
    int i = 0;
    for (; buffer[i].a[0] != '\0'; i++)
    {
        fprintf(fp, "%s %d %d %d\n", buffer[i].a, buffer[i].appointment.month, buffer[i].appointment.day, buffer[i].appointment.time);
    }
    fclose(fp);
    return 0;
}

//O tiedostonimi, tulostaa kalenterin, -> tiedostosta korvaten muistissa mahdollisesti olleen kalenterin
//read tiedosto
struct what_to_do *funktio_O(char *filename)
{
    FILE *fp = fopen(filename, "r");
    struct what_to_do *new_file = malloc(sizeof(struct what_to_do));
    new_file[0].a[0] = 0;

    if (!fp)
    {
        printf("error too");
        return NULL;
    }
    char *buffer = malloc(500 * sizeof(char));
    int i = 0;
    while (fgets(buffer, 50, fp) != NULL)
    {
        new_file = funktio_A(new_file, buffer);
        printf("%s %d %d %d\n", new_file[i].a, new_file[i].appointment.month, new_file[i].appointment.day, new_file[i].appointment.time);
        i++;
    }

    /*for (int j = 0; new_file[j].a[0] != 0; j++)
    {
        printf("%s %d %d %d\n", new_file[j].a, new_file[j].appointment.month, new_file[j].appointment.day, new_file[j].appointment.time);
    }*/

    free(buffer);
    fclose(fp);
    return new_file;
   
}

//Q, poistu

int funktio_Q(struct what_to_do* pointer)
{
    free(pointer);
    return -1;
}

//toimiva main

int main()
{
    char todo;
    struct what_to_do *array = malloc(sizeof(struct what_to_do));
    char testfile, testfile2;

    int count = 0;

    array[0].a[0] = 0;
    array[0].appointment.month = 0;
    array[0].appointment.day = 0;
    array[0].appointment.time = 0;
    printf("hei\n");
    int k = 1;
    while (k == 1)
    {
        scanf("%s", &todo);
        if (todo == 'A')
        {
            array = funktio_A(array, NULL); 
        }
        if (todo == 'D')
        {
            array = funktio_D(array);
        }
        if (todo == 'L')
        {
            while(array[count].a[0] != '\0'){
                count++;
            }
            qsort(array, count, sizeof(struct what_to_do), funktio_L);

            for (int j = 0; array[j].a[0] != '\0'; j++)
            {
                printf("%s %d.%d. klo %d\n", array[j].a, array[j].appointment.day, array[j].appointment.month, array[j].appointment.time);
            }
        }
        if (todo == 'W')
        {
            scanf("%s\n", &testfile);
            funktio_W(&testfile, array);
            k = 1;
        }
        if (todo == 'O')
        {
            scanf("%s", &testfile2);
            free(array);
            array = funktio_O(&testfile2);
            k = 1;
        }
        if (todo == 'Q')
            k = funktio_Q(array);
    }

    //free(array);
    //free(array2);

    return 0;
}